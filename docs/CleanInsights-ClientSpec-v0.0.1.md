<!-- Output copied to clipboard! -->


**Clean Insights \
Client Specification v0.0.1 \
August 2020**


[TOC]



## Overview

Data collection has become the default for most companies. But, like the fossil fuels that keep your car running, the data that keep a company running can lead to harmful side effects, leading some to call data a toxic element. The most privacy-conscious companies have proposed a simple solution to the problem: stop collecting data altogether. But ending all data collection means companies are “flying blind,” an unacceptable trade-off for most organizations. How, then, can campaign managers and product developers strike the right balance between promoting user privacy and promoting product success?

Clean Insights gives developers a way to plug into a secure, private measurement platform. It is focused on assisting in answering key questions about app usage patterns, and not on enabling invasive surveillance of all user habits. CI has levers to pull to cater to specific use cases and privacy needs. CI also provides methods for user interactions that are empowering instead of alienating.


## Practical Techniques

The Clean Insights projects utilizes practical techniques, distributed as a new kind of measurement SDK for connected devices and server infrastructure, that follows these core tenets:



*   **Data minimization**: only the minimum amount of usage and behavioral data should be gathered to answer a determined set of questions. The frequency, range, and level of details of measurements should be as small as possible.
*   **Source aggregation**: Possibly identifying data should not be held in any part of the system longer than necessary, aggregated at the source at the earliest possible time.
*   **Randomization**: Remove the link between the data and the individual by introducing noise to the data, to be sufficiently uncertain to unlink from a specific individual.
*   **Generalization**: Dilute the attributes of data subjects by modifying the respective scale or order of magnitude (i.e. a region rather than a city, a month rather than a week).
*   **Transparency**: Always get consent, and the scope of the data collection and algorithms used should be made publicly available and well explained.

Our approach defends from a variety of tracking and deanonymization attacks:



*   **Singling out**, which corresponds to the possibility to isolate some or all records which identify an individual in the dataset
*   **Linkability**, which is the ability to link, at least, two records concerning the same data subject or a group of data subjects
*   **Inference**, which is the possibility to deduce, with significant probability, the value of an attribute from the values of a set of other attributes.

Clean Insights also aims to improve the security of the measurement process by defending against the following threats:



*   **Surveillance & Censorship** (DoS, Information Disclosure, Repudiation): Traffic Analysis can identify users of a specific app based on the inclusion of a certain instance of an analytics package, connections to a specific endpoint, or traffic fingerprinting
*   **Increased Vulnerabilities** (Spoofing, Tampering, Elevation of Privs): Inclusion of unvetted, insecure third-party analytics libraries can lead to backdoors in software, man in the middle attacks through weak network security, and a general broader set of attack vectors for someone seeking unauthorized access to data and services.
*   **Weaponization of Users** (Tampering, DoS): Traffic from a set of users can be redirect and effectively weaponized to perform a DDoS attack against any desired target.


## Implementation Details

Implementation of a Clean Insights client library for measurement must abide by the guidelines outlined below. Thresholds are the means by which data collection is minimized, to ensure measurement only occurs during specific states. Aggregators provide the features for source aggregation, randomization and generalization. 

![](spec.png)

### Thresholds

Traditionally, analytics systems have centered around the direct registration of taps, clicks, page views and a few other set of events. In our implementation, before any measurement is collected, it must be considered if the current state is within the bounds of defined thresholds.

*   **Value Bounds Threshold**: For the specific event being measured, determine if it is within bounds set by the developer. For instance, if downloads speeds are being measured, only log an event when it is below a certain threshold of acceptable behavior. Another example is counting the uses of a specific feature in the app to know if the user exceeds a threshold. In that case a binary value true/false would be recorded.
*   **Session Length Threshold**: Only record values if the user's active use of an app exceeds interaction over a certain lenght of time "More than 10 minutes" or "More than 10 minutes in 1 day".

*   **Geospatial / Geofence Theshold**: If the app has location permission enabled, only record values if the user is within a defined set of geofence locations.

### Generalization

*   **Measurement Average**: Create a "running average" of values measured over a defined period of time, or defined number of measurements. 

*   **Deresolution**: Reduce the specifity of a set of measurements, such as location, so instead of a precise point, the nearest city or town is used instead.

*   **Noise Injection**: Add acceptable amount of random +/- noise into any measurement.

### Storage 

* Local persistence 

### Transmission

*   **Transmit Time Interval (Day, Week, Month)**: Configure the time interval for batching of measurements before they are transmitted to the server.
*   **TLS Hardening**: It should be possible for a CI SDK user to enable hardened TLS settings that align with their backend server.
*   **Certificate Pinning**: Since a known specific backend platform will always be used with a Clean Insights implementation, it should be possible to enable TLS Certificate Pinning. This defends against traffic interception of a few varieties. 
*   **Traffic Proxying and Obfuscation**: As a potential feature, we should explore the ability to proxy traffic through HTTPS-capable proxy servers, utilize Domain Fronting to connect to a known back-end, or integrate a Pluggable Transport system to defend against network filtering and surveillance. This could also include support for Tor, if available on a device, to use a Tor Onion Service address as the back-end server location.

### Consent

* All measurement must be done as "opt-in", though the request for measurement may happen whenever the developer chooses (not required to be immediate at onboarding)
* Threshold filters can be running before consent is acquired - consent is only needed once a threshold bound is met, and data is to be recorded
* 

## Thoughts on Class Structure

![](classdiagrams.png)


## Developer Experience
*   **Privacy-preserving Defaults**: While we can't stop developers from using this framework/SDK incorrectly, we can ensure that we have sane defaults and sample code. The focus should be on measuring a specific event in the app, and not all taps, navigation and other UI usage. There should be some instance of a Generalizer in-place to reduce resolution and traceability of any measurement. A simple opt-in consent prompt must be implemented. Storage and Transmission must be configured with a send interval of an hour or a day, at minimum. 
*   **Demonstrate best practice use in sample code**: Multiple sample projects or example code should be provided that show to both utilize default functionality provided, as well as implement new Thresholds and Generalizers. We must show both very common needs ("How often does the user utilize this feature?") and more complex ones ("Average session length over one week, only if it exceeds 10 minutes"). 
*   **Provide developer-as-user stories laying out end to end implementation**: It is important we also provide narrative description of our intended uses, how a developer and their team can go from a measurement to producing useful insights. We must also trace the data path from client SDK to server storage to analysis and insight generation.

## Questions about Clean Insights


**How are we pulling this off? (tracking data but not people)**

*   In short, we are helping limit what is collected and when it is collected, while also providing capabilities to process measurements on the client-side instead of sending raw data to a server. 

**What are we doing with IP addresses?**

*   In the client apps, we do not use or collect any unique identifiers tied to “real” device identifiers, names, emails or other contact information. On the server-side, we will provide methods for removing IP address logging from any measurement infrastructure. You can read our blog post on[ Tracking usage without tracking people](https://guardianproject.info/2017/06/08/tracking-usage-without-tracking-people/) for specific information on how to do this today.

**Is there any risk for my users? Could the data collected on their device put them in danger in any way?** **Are you collecting data points that could be corroborated to reveal personal identifying information on a user?  **

*   Clean Insights is much more than just code and tools. It is an entire approach to measurement that is opposed to the usual method of “damn the users, and collect everything you possibly can” that most web and app analytics services take. This includes teaching developers how to design threat models as part of planning what they are going to measure, and why they are going to do it. Through this, we strive to help developers mitigate putting their users in danger as much as possible.

**Deanonymization - How are we handling this potential?**

*   Part of this is answered in the previous questions, but we can expand here. There are two approaches possible with Clean Insights. First, through client-side processing of measurement data to produce aggregate “insights”, we move away from users deanonymizing themselves through uploading vast amounts of data. Second, through advanced cryptographic techniques like Differential Privacy and Private Join-and-Compute, we can offer computational proven methods for combatting deanonymization.

**My app doesn’t ask for trust. We demonstrate it. How does Clean Insights align with this value?**

*   Through transparency in code, process and implementation. The goal is to be able to share publicly any measurements you are gathering.

**How would Clean Insights build off of existing methodologies that have been worked on in other successful projects?**

*   Prototype for Android exists, and was integrated with OpenArchive mobile application as part of that process to safely measure media content types being shared
*   User measurement through Server infrastructure measurement has occurred through collaboration with Rights Action Lab, and will build upon concepts pioneered there

**What makes Clean Insights metrics distinct from other forms of measurement?**

*   completely open-source and decentralized, and can be self-hosted in a variety of deployment configurations
*   provides insights to app developers and designers about usage of their apps without resorting to full surveillance-style tracking as provided by Google Analytics and others
*   has an appropriate threat model baked in from the beginning
*   Focused on on building trust, engagement by users to make the apps better in a collaborative and not extractive way

**How do you envisage other tool teams using this methodology and integrating into their products?**

*   Through software libraries, guidelines, methodologies we publish
*   Through direct collaboration, help in implementation through developer support in both community channels, and professional service engagement opportunities
*   Through adoption by other existing partner projects and collaborations

**How can we incentivize tool teams to adopt this methodology?**

*   Require any measurement for reporting to be done in privacy preserving way, with guidelines regarding retention, data minimization practices, etc “HIPAA for Human Rights”
*   Community Awareness: Highlight the security, privacy, dignity issues that most app analytics can cause

**What are the hopes for long-term impact?**

*   Mass industry adoption in the way our SQLCipher library has
*   Conceptual impact through more mainstream privacy preserving features
*   More funding through other sources interested in extended this work, allowing us to fully implement differential privacy, other advanced features
